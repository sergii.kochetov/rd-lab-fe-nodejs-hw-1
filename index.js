const express = require('express');
const fs = require('fs');
const morgan = require('morgan');
const path = require('path');
const { filesRouter } = require('./filesRouter.js');

const app = express();
const PORT = 8080;
const accessLogStream = fs.createWriteStream(
  path.join(__dirname, 'access.log'),
  { flags: 'a' }
);

app.use(morgan('tiny', { stream: accessLogStream }));

app.use(express.json());

app.use('/api/files', filesRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

//ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error('err');
  res.status(500).send({ message: 'Server error' });
}
