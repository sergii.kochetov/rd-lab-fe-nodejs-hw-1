const fs = require('fs');
const path = require('path');

const PATH_TO_FILES = path.join(__dirname, 'files');
const supportedExt = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];

const createFile = (req, res) => {
  const { filename, content } = req.body;

  if (!filename || !content) {
    const wrongParam = !filename ? 'filename' : 'content';
    res.status(400).send({
      message: `Please specify '${wrongParam}' parameter`,
    });
  } else {
    const supportedExt = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
    const fileExt = path.extname(filename);
    const isSupported = supportedExt.find((item) => item === fileExt);

    if (!isSupported) {
      res.status(400).send({
        message: 'Incorrect file extension',
      });
    } else {
      const filePath = path.resolve(PATH_TO_FILES, filename);
      fs.writeFile(filePath, content, (err) => {
        if (err) {
          res.status(400).send({
            message: 'Server error',
          });
        }
        res.status(200).send({ message: 'File created successfully' });
      });
    }
  }
};

const getFiles = (req, res) => {
  fs.readdir(PATH_TO_FILES, (err, files) => {
    if (err) {
      res.status(500).send({
        message: 'Server error',
      });
    }
    res.status(200).send({
      message: 'Success',
      files: files,
    });
  });
};

const getFile = (req, res) => {
  const filename = req.params.filename;
  const filePath = path.join(PATH_TO_FILES, filename);

  fs.readFile(filePath, 'utf-8', (err, content) => {
    if (err) {
      res.status(400).send({
        message: `No file with '${filename}' filename found`,
      });
    } else {
      fs.stat(filePath, (err, data) => {
        if (err) {
          res.status(500).send({
            message: `Server error`,
          });
        } else {
          res.status(200).send({
            message: `Success`,
            filename: `${filename}`,
            content: `${content}`,
            extension: `${path.extname(filename).substring(1)}`,
            uploadedDate: `${data.birthtime}`,
          });
        }
      });
    }
  });
};

const editFile = (req, res) => {
  const filename = req.params.filename;
  const content = req.body.content;

  const filePath = path.join(PATH_TO_FILES, filename);

  fs.readFile(filePath, 'utf-8', (err) => {
    if (err) {
      res.status(400).send({
        message: `No file with '${filename}' filename found`,
      });
    } else {
      fs.writeFile(filePath, content, 'utf-8', (err) => {
        if (err) {
          res.status(500).send({
            message: `Server error`,
          });
        }

        res.status(200).send({
          message: 'File has been updated successfully',
        });
      });
    }
  });
};

const deleteFile = (req, res) => {
  const filename = req.params.filename;
  const filePath = path.join(PATH_TO_FILES, filename);

  fs.readFile(filePath, 'utf-8', (err) => {
    if (err) {
      res.status(400).send({
        message: `No file with '${filename}' filename found`,
      });
    } else {
      fs.unlink(filePath, (err) => {
        if (err) {
          res.status(500).send({
            message: `Server error`,
          });
        }

        res.status(200).send({
          message: 'File has been deleted successfully',
        });
      });
    }
  });
};

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile,
};
